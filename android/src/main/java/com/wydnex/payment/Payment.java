package com.wydnex.payment;


import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;

import org.json.JSONException;

@NativePlugin
public class Payment extends Plugin {


    @PluginMethod
    public void iniciarSdkPayment(PluginCall call) {
        this.getActivity().setTheme(com.getcapacitor.android.R.style.AppTheme);
        String publicKey = call.getString("publicKey");
        String apiServerNamePayment = call.getString("apiServerNamePayment");
        String apiServerName = call.getString("apiServerName");
        String authUser = call.getString("authUser");
        String authPassword = call.getString("authPassword");
        String moneda = call.getString("moneda");
        Double monto = call.getDouble("monto");
        MetodosGlobales globales = new MetodosGlobales(
                getContext(),
                call,
                getActivity().getSupportFragmentManager(),
                apiServerName,
                apiServerNamePayment,
                publicKey,
                authUser,
                authPassword,
                monto,
                moneda
        );
        JSObject res = globales.configurarSdk();
        if (res.getBool("estado")) {
            globales.iniciarPago();
        } else {
            call.resolve(res);
        }

    }

}

