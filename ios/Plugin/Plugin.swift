import Foundation
import Capacitor
import LyraPaymentSDK


/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitorjs.com/docs/plugins/ios
 */
@objc(Payment)
public class Payment: CAPPlugin {
    
         var publicKey = "";
         var apiServerNamePayment = "";
         var apiServerName = "";
         var authUser = "";
         var authPassword = "";
         var moneda = "";
         var monto = 0.0;
        
    
    @objc func iniciarSdkPayment(_ call: CAPPluginCall) {
     
        let publicKey = call.getString("publicKey");
        let apiServerNamePayment = call.getString("apiServerNamePayment");
        let apiServerName = call.getString("apiServerName");
        let authUser = call.getString("authUser");
        let authPassword = call.getString("authPassword");
        let moneda = call.getString("moneda");
        let monto = call.getDouble("monto");
        
         self.publicKey = publicKey!
         self.apiServerNamePayment = apiServerNamePayment!
         self.apiServerName = apiServerName!
         self.authUser = authUser!
         self.authPassword = authPassword!
         self.moneda = moneda!
         self.monto = monto!
        
        let resp = initSDK()
        let bol = resp["estado"] as! Bool
        if bol == true {
            inciarPago(call:  call)
        }else {
            call.success(resp)
        }
        
        
    }
    
    func initSDK()->[String:Any]{
        var rspta : [String:Any] = ["estado":false,"mensaje":"","payload":""]
        
        var configurationOptions = [String : Any]()
        configurationOptions[Lyra.apiServerName] = self.apiServerNamePayment
        
        //Initialize Payment SDK
        do {
            rspta["estado"] = true
            rspta["mensaje"] = "Ok"
            try Lyra.initialize(self.publicKey, configurationOptions)
            return rspta
        } catch  {
            rspta["estado"] = true
            rspta["mensaje"] = "No se inicalizo el SDK"
            rspta["payload"] = "No se inicalizo el SDK"

            return rspta
        }
    }
    
    
    func inciarPago(call: CAPPluginCall){
        let rspta : [String:Any] = ["estado":false,"mensaje":"","payload":""]
        let formTokenVersion = Lyra.getFormTokenVersion()
        let amountString = String(format: "%03d",  Int(self.monto*100) )
        let params = [
            "formTokenVersion": formTokenVersion,
            "amount" : amountString,
            "currency": self.moneda
            ] as Dictionary<String, Any>
        let username = self.authUser
        let password = self.authPassword
        let base64encoded = "\(username):\(password)".data(using: .isoLatin1)?.base64EncodedString() ?? ""
        var request =
            URLRequest(url: URL(string: self.apiServerName + "/createPayment")!)

        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Basic \(base64encoded)", forHTTPHeaderField: "Authorization")
        
        let session =   URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
      
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                let jsonData = try! JSONSerialization.data(withJSONObject: json, options: [])
                let decoded = String(data: jsonData, encoding: .utf8)!
                print(decoded)
                self.initFormPago(payload: decoded,call: call)
            } catch {
                call.success(rspta)
            }
            })
      
        task.resume()
    }
    
    
    
    func initFormPago(payload:String,call: CAPPluginCall){
        var rspta : [String:Any] = ["estado":false,"mensaje":"","payload":""]
        
        let _: () = try!  Lyra.process(self.bridge.viewController, payload, onSuccess: { (LyraResponse) in
            self.verificarPago(res: LyraResponse,call: call)
            
        }) { (LyraError, LyraResponse) in
            rspta["mensaje"] =  LyraError.errorMessage
            rspta["payload"] = LyraError.errorMessage
            call.success(rspta)
        }
        
        
        
    }
    func verificarPago(res:LyraResponse,call: CAPPluginCall){
          var rspta : [String:Any] = ["estado":false,"mensaje":"","payload":""]
        let base64encoded = "\(self.authUser):\(self.authPassword)".data(using: .isoLatin1)?.base64EncodedString() ?? ""
        var request =
            URLRequest(url: URL(string: self.apiServerNamePayment+"/verifyResult")!)
        request.httpMethod = "POST"
        request.httpBody = res.getResponseData()
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Basic \(base64encoded)", forHTTPHeaderField: "Authorization")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in

                rspta["estado"] = true
                rspta["payload"] = res.getResponseDataString()
                call.success(rspta)
          
        })
        
        task.resume()
    }
    
}
